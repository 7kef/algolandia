# Algolandia

### Algolandia - – is the game created as a student’s team project. It should help students or basically everyone who is interested in learning algorithms. It aims to deliver an interactive method for learning algorithms and data structures.

### The project is written in  ```Python``` with the help of ```pygame``` library.

### Algolandia currently has one level, wich helps algorithm beginners to learn about *Bubble Sort* algorithm and **Depth-first search** algorithm. 

### The program is exported into an executable .exe file, which means you don't need to have ```Python``` and ```pygame``` installed to open it. 

## If you want just to get this **game itself** (without all source files), **download** only **Algolandia** directory, and find the *Algolandia.exe* file in it. 

