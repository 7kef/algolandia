import pygame, sys, os, random
import Globals as gl
from Text import Text
from Button import Button
from Gracz import Player
import Platform
import mini_gra1 as mini1
import mini_gra2 as mini2

cen = gl.menu_container.get_rect()
cen.center = gl.game_screen.get_rect().center
  
##name = Text('Shadow Runner', (gl.game_screen_width//2, 0.15*gl.game_screen_height),
##            int(7*((gl.game_screen_height+gl.game_screen_width)/100)),
##                        pygame.color.THECOLORS['gray15'], 
##                        font='chiller')


##start = Button(position=(gl.game_screen_width//2,0.4*gl.game_screen_height), width=0.2*gl.game_screen_width,height=0.1*gl.game_screen_height,text='Start',
##                         function=('start_game',False),
##                         background = pygame.color.THECOLORS['darkgreen'],
##                         on_hover = pygame.color.THECOLORS['green'],
##                         font_size = int(3*((gl.game_screen_height+gl.game_screen_width)/100)),
##                         text_color = pygame.color.THECOLORS['orange'],
##                         font = 'chiller')  
##cen_start = gl.menu_start.get_rect()
##cen_start.center = gl.game_screen.get_rect().center
start = Button(position=(cen.centerx+25, cen.centery-75),
                function=('start_game',False),
                background = gl.menu_start,
                on_hover = gl.menu_start_hover,
)  

exitbutton = Button(position=(cen.centerx+25, cen.centery+100),
                function=('close',False),
                background = gl.menu_quit,
                on_hover = gl.menu_quit_hover,
)       
        
##exitbutton = Button(position=(gl.game_screen_width//2,0.8*gl.game_screen_height), width=0.2*gl.game_screen_width,height=0.1*gl.game_screen_height,text='Quit', function=('close', False),
##                         background = pygame.color.THECOLORS['brown4'],
##                         on_hover = pygame.color.THECOLORS['brown1'],
##                         font_size = int(3*((gl.game_screen_height+gl.game_screen_width)/100)),
##                         text_color = pygame.color.THECOLORS['black'],
##                         font = 'chiller')




gratulations = Text('Gratulacje! Wygrałeś! :) ',
            int(4*((gl.game_screen_height+gl.game_screen_width)/100)),
                        pygame.color.THECOLORS['gray15'],
                        font=gl.font_bold)
gratulations.rect.center = (gl.game_screen_width//2, gl.game_screen_height//2)

gl.player1 = Player(gl.player_stand_list,gl.player_walk_right, gl.player_walk_left,
                 gl.player_jump_list_right, gl.player_jump_list_left, gl.player_dead_right, gl.player_dead_left)
##gl.player1.rect.bottom = 0
##gl.player1.rect.left = 7200
##gl.current_level = Platform.Level1(gl.player1)
##gl.player1.level = gl.current_level


Labirynth = random.choice([mini1.L1,mini1.L2,mini1.L3,mini1.L4])
game = None
game2 = None

while True:
    #gl.game_screen.fill(pygame.color.THECOLORS['lightblue'])
    if gl.game_menu:
       gl.game_screen.blit(gl.menubg_image,[0,-300])
       
       gl.game_screen.blit(gl.menu_container,cen)
       
       #name.draw(gl.game_screen,'center')
       start.place('true')
       #preferences.place('true')
       exitbutton.place('true')
    else:
            
        if gl.player1.points == 30:
            gl.current_level.draw(gl.game_screen)
            gratulations.draw(gl.game_screen, 'center')
            #gl.game_screen.blit()
            pygame.display.flip()
            pygame.time.delay(3000)
            gl.game_menu = True
            
        if not gl.minigame1 == 'a' and not gl.minigame2 == 'a':
            gl.current_level.draw(gl.game_screen)
            gl.current_level.update()
            gl.player1.draw(gl.game_screen)
            gl.player1.update()
        elif gl.minigame1 == 'a':
            if game is None:
                game = mini1.Game(mini1.Player(mini1.player_image), Labirynth)
            #gl.game_screen.fill(pygame.color.THECOLORS['lightgreen'])
            game.update()
            game.draw(gl.game_screen)
            restart = False
            cor = game.player.coordinates
            if cor in game.discovered_path or cor == game.path[-1]:
                if cor not in game.discovered_path:
                    game.discovered_path.append(game.path.pop())
            else:
                pygame.display.flip()
                pygame.time.delay(500)
                game._set_attributes()
                game.player.coordinates = (1,0)
                game.player.rect.center = [410, 270]
                game.player.image = game.player.image_list[0]
                for plate in game.set_of_plates:
                    plate.covered = True
                restart = True
                gl.game_screen.blit(mini1.background, [0,0])
                mini1.text1.draw(gl.game_screen)
                mini1.text2.draw(gl.game_screen)
                #print('Koniec mini gry');
            
            #aktualizacja okna pygame
            pygame.display.flip()
            if restart:
                pygame.time.delay(3000)
                restart = False
            if cor == (8,17):
                # tu trzeba wrócić do głownej gry
                gl.game_screen.blit(mini1.background, [0,0])
                mini1.text3.draw(gl.game_screen)
                pygame.display.flip()
                pygame.time.delay(3000)
                gl.game_screen.blit(mini1.background, [0,0])
                mini1.text4.draw(gl.game_screen)
                pygame.display.flip()
                pygame.time.delay(2000)
                gl.player1.points += 10
                gl.minigame1 = 'c'
                #print('Koniec mini gry');
            
            #aktualizacja okna pygame
            pygame.display.flip()
            if restart:
                pygame.time.delay(3000)
                restart = False

            
            
        elif gl.minigame2 == 'a':
            #gl.game_screen.fill(pygame.color.THECOLORS['lightgray'])
            # tu koniec gry!!! (wyście do głównej gry)
            if game2 is None:
                game2 = mini2.Game(mini2.Frame(mini2.frame_image))
            if game2.order_bubble_sort_list[-1] == -1 and\
               game2.find_letter(game2.last).movement_x == 0:
                mini2.text3.draw(gl.game_screen)
                pygame.display.flip()
                pygame.time.delay(2000)
                gl.game_screen.blit(mini2.background, [0,0])
                mini2.text4.draw(gl.game_screen)
                pygame.display.flip()
                pygame.time.delay(2000)
                gl.player1.points += 10
                gl.minigame2 = 'c'

            if game2.game_ok:
                game2.update()
                game2.draw(gl.game_screen)
            elif game2.order_bubble_sort_list[-1] != -1:
                pygame.time.delay(500)
                gl.game_screen.blit(mini2.background, [0,0])
                mini2.text1.draw(gl.game_screen)
                mini2.text2.draw(gl.game_screen)
                random.shuffle(game2.letters)
                game2.find_bubble_sort_list()
                game2.game_ok = True
                game2.frame.rect.x = 533
                game2.frame.rect.y = 179
                game2.frame.position = 0
                pygame.display.flip()
                pygame.time.delay(3000)
        

            #aktualizacja okna pygame
            pygame.display.flip()
            
    #listening for event
    for event in pygame.event.get():
        
        #window close button
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                pygame.quit()
                sys.exit()

        if game:
            game.get_event_player(event)
        if game2:
            game2.get_event(event)
        gl.player1.get_event(event)


    
    pygame.display.update()
    gl.clock.tick(60)

    

