# -*- coding: utf-8 -*-
"""
Created on Wed May 23 01:24:14 2018

@author: vh-edu
"""
import pygame, time, random, sys, os

pygame.init()

pygame.display.set_caption("Algolandia")
clock = pygame.time.Clock()
screen = pygame.display.Info()
pause = False
game_menu = True
game_start = False
game_exit = False
minigame1 = 'n'  # 'n' - not started, 'c' - completed, 'a' - active
minigame2 = 'n'  # 'n' - not started, 'c' - completed, 'a' - active

player1 = None
current_level = None
font = 'LithosPro-Regular.otf'
font_bold = 'LithosPro-Bold.otf'
total_points = 0
#setting the window size 80%wight*80%height

#game_screen = pygame.display.set_mode((int(screen.current_w*0.9), int(screen.current_h*0.9)),
#                                         0,
                                        #pygame.FULLSCREEN |
                                      # pygame.RESIZABLE,
#                                      32)

#VGA
#game_screen = pygame.display.set_mode((640,480),0,32)
#XGA
#game_screen = pygame.display.set_mode((1024,768),0,32)
#HDTV
#game_screen = pygame.display.set_mode((1366,768),0,32)
#FULLHD
#game_screen = pygame.display.set_mode((1920,1080),pygame.FULLSCREEN | pygame.HWSURFACE,32)
game_screen_width = game_screen.get_rect().width
game_screen_height = game_screen.get_rect().height

gameDisplay = (game_screen_width, game_screen_height)

#game background
level1bg = pygame.image.load(os.path.join(os.getcwd(), 'img','levels','tlo.jpg')).convert()






#player graphics
player_file_list = sorted(os.listdir(os.path.join('img','player')))
player_mini = pygame.image.load(os.path.join('img','player','red_mini.png')).convert_alpha()
player_walk_right = [pygame.image.load(os.path.join('img','player',name)).convert_alpha()
                     for name in player_file_list if "walk_R" in name]
player_walk_left = [pygame.image.load(os.path.join('img','player',name)).convert_alpha()
                    for name in player_file_list if "walk_L" in name]
player_stand_list = [pygame.image.load(os.path.join('img','player',name)).convert_alpha()
                     for name in player_file_list if "stand" in name]
player_jump_list_right = [pygame.image.load(os.path.join('img','player',name)).convert_alpha()
                          for name in player_file_list if "jump_R" in name]
player_jump_list_left = [pygame.image.load(os.path.join('img','player',name)).convert_alpha()
                         for name in player_file_list if "jump_L" in name]
player_dead_right = [pygame.image.load(os.path.join('img','player',name)).convert_alpha()
                     for name in player_file_list if "dead_R" in name]
player_dead_left = [pygame.image.load(os.path.join('img','player',name)).convert_alpha()
                    for name in player_file_list if "dead_L" in name]


###platform graphics
##platform_file = sorted(os.listdir(os.path.join('img','platforms')))
##platform_left = [pygame.image.load(
##    os.path.join('img','platforms',name)).convert_alpha(level1bg)
##                 for name in platform_file if "platf_lewy" in name]
##platform_right = [pygame.image.load(
##    os.path.join('img','platforms',name)).convert_alpha(level1bg)
##                  for name in platform_file if "platf_prawy" in name]
##platform_middle = [pygame.image.load(
##    os.path.join('img','platforms',name)).convert_alpha(level1bg)
##                   for name in platform_file if "platf_srodek" in name]

# solid
solid_file = sorted(os.listdir(os.path.join('img','platforms','solid')))
solid_bottom = [pygame.image.load(
    os.path.join('img','platforms','solid', name)).convert()
                   for name in solid_file  if "bottom" in name]
solid_top = [pygame.image.load(
    os.path.join('img','platforms','solid', name)).convert()
                   for name in solid_file  if "top" in name]


solid_bottomL = solid_bottom[:2] + solid_bottom[1:2]*2
solid_bottomR = solid_bottom[1:2]*3 + solid_bottom[3:]
solid_bottomM = solid_bottom[1:2]*4

solid_topL = solid_top[:2] + solid_top[1:2]*2
solid_topR = solid_top[1:2]*3 + solid_top[3:]
solid_topM = solid_top[1:2]*4

# main platform

floor_file = sorted(os.listdir(os.path.join('img','platforms','bottom')))
main_platform_bottom = [pygame.image.load(
    os.path.join('img','platforms','bottom', name)).convert_alpha()
                   for name in floor_file  if "bottom" in name]

main_platform_bottomL = main_platform_bottom[:8] + main_platform_bottom[4:8] + main_platform_bottom[1:4]
main_platform_bottomR = main_platform_bottom[4:8]*2 + main_platform_bottom[8:12]*2
main_platform_bottomM = main_platform_bottom[4:8]*4

top_file = sorted(os.listdir(os.path.join('img','platforms','top')))
main_platform_top = [pygame.image.load(
    os.path.join('img','platforms','top', name)).convert_alpha()
                   for name in top_file  if "top" in name]

main_platform_topL = main_platform_top[:8] + main_platform_top[4:8] + main_platform_top[1:4]
main_platform_topR = main_platform_top[4:8]*2 + main_platform_top[8:12]*2
main_platform_topM = main_platform_top[4:8]*4

hang_file = sorted(os.listdir(os.path.join('img','platforms','hang')))
hanging_platform =[pygame.image.load(
    os.path.join('img','platforms','hang', name)).convert_alpha()
                   for name in hang_file  if "plat" in name]

hanging_platformL = hanging_platform[:8] + hanging_platform[4:8] + hanging_platform[1:4]
hanging_platformR = hanging_platform[4:8]*2 + hanging_platform[8:12]*2
hanging_platformM = hanging_platform[4:8]*4

briar_file = sorted(os.listdir(os.path.join('img','platforms','briars')))
briar =[pygame.image.load(
    os.path.join('img','platforms','briars', name)).convert_alpha()
                   for name in briar_file  if "briars" in name]
briarT = [briar[2]]*16
briarB = [briar[0]]*6 + [briar[1]]*6



#enemies graphics
platform_file = sorted(os.listdir(os.path.join('img','enemies')))
enemyA_left = [pygame.image.load(os.path.join('img','enemies', name)).convert_alpha()
               for name in platform_file if "enemyA_L" in name]
enemyA_right = [pygame.image.load(os.path.join('img','enemies',name)).convert_alpha()
               for name in platform_file if "enemyA_R" in name]
enemyB_left = [pygame.image.load(os.path.join('img','enemies', name)).convert_alpha()
               for name in platform_file if "enemyB_L" in name]
enemyB_right = [pygame.image.load(os.path.join('img','enemies',name)).convert_alpha()
               for name in platform_file if "enemyB_R" in name]

##platform_top_file = sorted(os.listdir(os.path.join('img','platforms','top')))
##top_left = [pygame.image.load(os.path.join('img','platforms','top',name)).convert_alpha(level1bg) for name in platform_top_file if "lewy" in name]
##top_right = [pygame.image.load(os.path.join('img','platforms','top',name)).convert_alpha(level1bg) for name in platform_top_file if "prawy" in name]
##top_middle = [pygame.image.load(os.path.join('img','platforms','top',name)).convert_alpha(level1bg) for name in platform_top_file
##              if "prawy" not in name and "lewy" not in name and "srodek" not in name and "solid" not in name]
##top_single = [pygame.image.load(os.path.join('img','platforms','top',name)).convert_alpha(level1bg) for name in platform_top_file if "srodek" in name]
##
##solid_list = [pygame.image.load(os.path.join('img','platforms','top',name)).convert_alpha(level1bg) for name in platform_top_file if "solid" in name]
##
##platform_start_file = sorted(os.listdir(os.path.join('img','platforms','start')))
##start_left = [pygame.image.load(os.path.join('img','platforms','start',name)).convert_alpha(level1bg) for name in platform_start_file if "lewy" in name]
##start_middle = [pygame.image.load(os.path.join('img','platforms','start',name)).convert_alpha(level1bg) for name in platform_start_file
##              if "prawy" not in name and "lewy" not in name and "srodek" not in name]
##start_right = [pygame.image.load(os.path.join('img','platforms','start',name)).convert_alpha(level1bg) for name in platform_start_file if "prawy" in name]
##
##
##
##bottom_left_file = sorted(os.listdir(os.path.join('img','platforms','bottom')))
##bottom_left = [pygame.image.load(os.path.join('img','platforms','bottom',name)).convert_alpha(level1bg) for name in bottom_left_file if "lewy" in name]
##bottom_middle = [pygame.image.load(os.path.join('img','platforms','bottom',name)).convert_alpha(level1bg) for name in bottom_left_file
##              if "prawy" not in name and "lewy" not in name and "srodek" not in name]
##bottom_right = [pygame.image.load(os.path.join('img','platforms','bottom',name)).convert_alpha(level1bg) for name in bottom_left_file if "prawy" in name]
##bottom_single = [pygame.image.load(os.path.join('img','platforms','bottom',name)).convert_alpha(level1bg) for name in bottom_left_file if "srodek" in name]
##


point_image = pygame.image.load(os.path.join('img','levels', 'moneta1.png')).convert_alpha()
menubg_image = pygame.image.load(os.path.join('img','StronaGlowna', 'tlo.png')).convert_alpha()
menu_container = pygame.image.load(os.path.join('img','StronaGlowna', 'glowna_kafelek_srodkowy_75.png')).convert_alpha()
menu_start = pygame.image.load(os.path.join('img','StronaGlowna', 'glowna_start_zgaszony_75.png')).convert_alpha()
menu_start_hover = pygame.image.load(os.path.join('img','StronaGlowna', 'glowna_start_podswietlony_75.png')).convert_alpha()
menu_quit = pygame.image.load(os.path.join('img','StronaGlowna', 'glowna_zakoncz_zgaszony_75.png')).convert_alpha()
menu_quit_hover = pygame.image.load(os.path.join('img','StronaGlowna', 'glowna_zakoncz_podswietlony_75.png')).convert_alpha()
minigra1_image = pygame.image.load(os.path.join('img','levels', 'minigra1.png')).convert_alpha()
minigra2_image = pygame.image.load(os.path.join('img','levels', 'minigra1.png')).convert_alpha()
