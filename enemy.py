import pygame, random
# ogólna klasa wroga
class Enemy(pygame.sprite.Sprite):
    def __init__(self, image_right, image_left, image_dead_right,
                 image_dead_left, platform = None, movement_x = 0, movement_y = 0):
        super().__init__()
        self.image = image_right[0]
        self.rect = self.image.get_rect()
        self.movement_x = movement_x
        self.movement_y = movement_y
        self.direction_of_movement = 'right'
        self.platform = platform
        self.image_right = image_right
        self.image_left = image_left
        self.image_dead_right = image_dead_right
        self.image_dead_left = image_dead_left
        self.lifes = 1
        self.count = 0

        if self.platform:
            self.rect.bottom = self.platform.rect.top
            self.rect.centerx = random.randint(
                self.platform.rect.left + self.rect.width,
                self.platform.rect.right - self.rect.width)


    def update(self):
        if not self.lifes and self.count > 7:
            self.kill()
            
        self.rect.x += self.movement_x
        self.rect.y += self.movement_y

        # animacje
        if self.lifes:
            if self.movement_x > 0:
                self._move(self.image_right)
            if self.movement_x < 0:
                self._move(self.image_left)
##        else:
##            if self.direction_of_movement == 'right':
##                self._move(self.image_dead_right)
##            else:
##                self._move(self.image_dead_left)
            

    def _move(self, image_list):
        if self.count < 4:
            self.image = image_list[0]
        elif self.count < 8:
            self.image = image_list[1]

        if self.count >= 8:
            self.count  = 0
        else:
            self.count += 1


class Platform_Enemy(Enemy):
    def update(self):
        super().update()
        if self.rect.left < self.platform.rect.left or\
           self.rect.right > self.platform.rect.right:
            self.movement_x *= -1

        if self.movement_x > 0 and  self.direction_of_movement == 'left':
            self.direction_of_movement = 'right'
    
        if self.movement_x < 0 and  self.direction_of_movement == 'right':
            self.direction_of_movement = 'left'
