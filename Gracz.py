# -*- coding: utf-8 -*-
"""
Created on Wed May 23 17:49:38 2018

@author: vh-edu
"""
import Globals as gl
import pygame, random
import Platform
from Text import Text

class Player(pygame.sprite.Sprite):
    def __init__(self, stand_list, right_image_list, left_image_list,
                 right_jump_list, left_jump_list, right_image_list_dead, left_image_list_dead):
        super().__init__()
        self.stand_image_list = stand_list
        self.right_image_list = right_image_list
        self.left_image_list = left_image_list
        self.right_jump_list = right_jump_list
        self.left_jump_list = left_jump_list
        self.right_image_list_dead = right_image_list_dead
        self.left_image_list_dead = left_image_list_dead
        self.image = self.stand_image_list[1]
        self.rect = self.image.get_rect()
        self.movement_x = 0
        self.movement_y = 0
        self.level = None
        self.direction_of_movement = 'right'
        self.on_platform = False
        self._count = 0
        self._count_d = 0
        self.lifes = 7
        self.points = 0
        self.dead = False
        self.bonus_lifes = 0

        self.gameover = Text('Niestety przegrałeś!',
            int(4*((gl.game_screen_height+gl.game_screen_width)/100)),
                        pygame.color.THECOLORS['gray15'], 
                        font=gl.font)
        self.gameover.rect.center = (gl.game_screen_width//2, gl.game_screen_height//2)

    def turn_left(self):
        self.direction_of_movement = 'left'
        self.movement_x = -6
    
    def turn_right(self):
        self.direction_of_movement = 'right'
        self.movement_x = 6
    
    def update(self):
        self._gravitation()
        if self._count_d > 0:
            self.movement_y = 0
        self.rect.x+=self.movement_x
        coliding_platforms = pygame.sprite.spritecollide(self, self.level.set_of_platforms, False)
        for platform in coliding_platforms:
            if self.movement_x > 0:
                self.rect.right = platform.rect.left
                
            if self.movement_x < 0:
                self.rect.left = platform.rect.right
        #animation
        if self.movement_x>0 and self.on_platform:
            self._move(self.right_image_list)
        elif self.movement_x<0 and self.on_platform:
            self._move(self.left_image_list)
        
        self.rect.y += self.movement_y
        coliding_platforms = pygame.sprite.spritecollide(self, self.level.set_of_platforms, False)
        for platform in coliding_platforms:
            if self.movement_y > 0:
                self.rect.bottom = platform.rect.top
                if self.direction_of_movement == 'right' and self.movement_x==0:
                    self.image = self.stand_image_list[1]
                elif self.movement_x==0:
                    self.image = self.stand_image_list[0]
                
            if self.movement_y < 0:
                self.rect.top = platform.rect.bottom
            self.stop_y()

            # gracz jedzie razem z platformą
            if isinstance(platform, Platform.MovingPlatform) and self.movement_x == 0:
                self.rect.x += platform.movement_x

        self.rect.y+=4
        if pygame.sprite.spritecollide(self, self.level.set_of_platforms, False):
            self.on_platform = True
        else:
            self.on_platform = False
        self.rect.y-=4
        
        #animation jump
        if self.direction_of_movement == 'right':
            if self.movement_y < -13:
                self.image = self.right_jump_list[1]
            elif self.movement_y < -10:
                self.image = self.right_jump_list[2]
            elif self.movement_y < -1:
                self.image = self.right_jump_list[3]
            elif self.movement_y > 3:
                self.image = self.right_jump_list[4]
        else:
            if self.movement_y < -13:
                self.image = self.left_jump_list[1]
            elif self.movement_y < -10:
                self.image = self.left_jump_list[2]
            elif self.movement_y < -1:
                self.image = self.left_jump_list[3]
            elif self.movement_y > 3:
                self.image = self.left_jump_list[4]
        self.rect.y+=20
        if pygame.sprite.spritecollide(self, self.level.set_of_platforms, False):
            if self.direction_of_movement == 'right' and self.movement_y>2:
                self.image = self.right_jump_list[5]
            elif self.movement_y>2:
                self.image = self.left_jump_list[5]
        else:
            self.on_platform = False
        self.rect.y-=20
        

        #minigry
        if pygame.sprite.spritecollide(self, self.level.set_of_minigames, True):
            if gl.minigame1 == 'n' and gl.minigame2 == 'n':
                number = random.choice([1,2])
                if number == 1:
                    gl.minigame1 = 'a'
                else:
                    gl.minigame2 = 'a'
            else:
                if gl.minigame1 == 'c':
                    gl.minigame2 = 'a'
                elif gl.minigame2 == 'c':
                    gl.minigame1 = 'a'
            #pygame.time.delay(1500)
            pygame.event.clear()
            



        #zbieranie punktów
        if pygame.sprite.spritecollide(
            self, self.level.set_of_points, True):
            self.points += 1
            if self.points>=10 and self.bonus_lifes==0:
                self.lifes +=1
                self.bonus_lifes +=1
            elif self.points>=20 and self.bonus_lifes==1:
                self.lifes +=1
                self.bonus_lifes +=1


        # sprawdzamy kolizję z wrogami lub spadek w przepaść
        if pygame.sprite.spritecollide(
            self, (set(self.level.set_of_enemies) | self.level.set_of_briars), False):
            self.dead = True
            if gl.player1.lifes == 0:
                gl.current_level.draw(gl.game_screen)
                self.gameover.draw(gl.game_screen, 'center')
                #gl.game_screen.blit()
                pygame.display.flip()
                pygame.time.delay(3000)
            
                gl.game_menu = True
        

        if self.dead:
            if self._count_d == 0:
                self._count_d += 1
                self.lifes -= 1
##                pygame.time.delay(500)
            if self._count_d == 43:
                self.rect.left = 20 + self.level.world_shift[0]
                self.rect.bottom = gl.game_screen_height + self.level.world_shift[1]
                self.dead = False
##        if self.rect.bottom > 100 + gl.game_screen_height + self.level.world_shift[1]:
##            if self._count_d == 0:
##                self._count_d += 1
##                self.lifes -= 1
####            pygame.time.delay(500)
##            if self._count_d == 43:
##                self.rect.left = 20 + self.level.world_shift[0]
##                self.rect.bottom = gl.game_screen_height + self.level.world_shift[1]

        # aniamcja śmierci
        if self._count_d > 0:
            if self.direction_of_movement == 'right':
                self._move_dead(self.right_image_list_dead)
            else:
                self._move_dead(self.left_image_list_dead)
    
        

        
    def jump(self):
        if self.on_platform:
            self.movement_y = -16
    
    def draw(self, surface):
        surface.blit(self.image, self.rect)
        # rysowanie żyć na ekranie
        if self.lifes:
            for i in range(self.lifes):
                surface.blit(gl.player_mini, [60 * i +20, 30])
        font=pygame.font.SysFont('calibri', 60)
        font.set_bold(True)
        surface.blit(font.render(str(self.points)+"/"+str(gl.total_points), 1, pygame.color.THECOLORS['darkgreen']), [gl.game_screen_width-300,70])
        surface.blit(gl.point_image, [gl.game_screen_width-110, 60])
        
        
    def get_event(self, event):
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                self.turn_left()
                
            if event.key == pygame.K_RIGHT:
                self.turn_right()

            if event.key == pygame.K_UP:
                self.jump()
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT and self.movement_x<0 :
                self.stop_x()
                if self.on_platform:
                    self.image = self.stand_image_list[0]
                
            if event.key == pygame.K_RIGHT and self.movement_x>0:
                self.stop_x()
                if self.on_platform:
                    self.image = self.stand_image_list[1]

            
                
    def stop_x(self):
        self.movement_x=0
    def stop_y(self):
        self.movement_y=0

    def _gravitation(self):
        if self.movement_y == 0:
            self.movement_y = 2
        else:
            self.movement_y+=0.3

    def _move(self,image_list):
        if self._count < 4:
            self.image = image_list[0]
        elif self._count<8:
            self.image = image_list[1]
        elif self._count<12:
            self.image = image_list[2]
        elif self._count<16:
            self.image = image_list[3]
        elif self._count<20:
            self.image = image_list[4]
        elif self._count<24:
            self.image = image_list[5]
        if self._count>=24:
            self._count = 0
        else:
            self._count+=1

    def _move_dead(self, image_list):
        if self._count_d < 8:
            self.image = image_list[0]
        elif self._count_d < 15:
            self.image = image_list[1]
        elif self._count_d < 22:
            self.image = image_list[2]
        elif self._count_d < 29:
            self.image = image_list[3]
        elif self._count_d < 36:
            self.image = image_list[4]
        elif self._count_d < 43:
            self.image = image_list[5]
##        elif self._count_d < 50:
##            self.image = image_list[6]
        if self._count_d >= 50:
            self._count_d = 0
        else:
            self._count_d +=1
            
